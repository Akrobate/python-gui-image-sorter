

import sys
import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw

class MainWindow(Gtk.ApplicationWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Things will go here
        image = Gtk.Image()
        image.set_from_file("test1.jpg")
        image.set_size_request(500, 700)

        box = Gtk.Box()
        box.append(image)
        self.set_child(box)

class MyApp(Adw.Application):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.connect('activate', self.on_activate)

    def on_activate(self, app):
        self.win = MainWindow(application=app)
        self.win.present()

app = MyApp(application_id="com.example.GtkApplication")
app.run(sys.argv)


'''
import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk

# Création de la fenêtre principale
window = Gtk.Window()
#window.connect("destroy", Gtk.main_quit)

# Création d'un widget d'image
image = Gtk.Image()
image.set_from_file("test1.jpg")  # Spécifiez le chemin vers votre image


# Création d'une boîte de conteneur
box = Gtk.Box()
#box.set_child(image)

# Ajout de la boîte de conteneur à la fenêtre principale
#window.set_child(box)

# Affichage de la fenêtre
window.show()

# Boucle principale de l'application
#Gtk.main()
'''