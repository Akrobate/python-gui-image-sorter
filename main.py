import tkinter as tk
from tkinter import filedialog
from PIL import ImageTk, Image, ImageOps
import os
from tkfilebrowser import askopendirname, askopenfilenames, asksaveasfilename
import random


def open_folder():
    folder_path = askopendirname()
    if folder_path:
        open_folder_with_file(folder_path)


def open_folder_with_file(folder_path):
    global image_files;
    if folder_path:
        image_files = [os.path.join(folder_path, filename) for filename in os.listdir(folder_path) if filename.endswith((".jpg", ".png", ".jpeg"))]
        if len(image_files) >= 2:
            load_images(image_files[0], image_files[1])
            display_images()
        else:
            print("Le dossier ne contient pas suffisamment d'images.")


def load_images(image1_path, image2_path):
    global loaded_image1, loaded_image2
    loaded_image1 = Image.open(image1_path)
    loaded_image2 = Image.open(image2_path)


def resize(event):
    global window_width, window_height
    if (window_width != event.width) and (window_height != event.height) and (loaded_image1 != 0) and (loaded_image2 != 0):
        window_width, window_height = event.width,event.height
        print(f"The width of Toplevel is {window_width} and the height of Toplevel "
                f"is {window_height}")
        display_images()


def display_images():
    resized_image1 = ImageOps.contain(loaded_image1, ((int(root.winfo_width() / 2)) , root.winfo_height()))
    resized_image2 = ImageOps.contain(loaded_image2, ((int(root.winfo_width() / 2)), root.winfo_height()))

    tk_image1 = ImageTk.PhotoImage(resized_image1)
    tk_image2 = ImageTk.PhotoImage(resized_image2)

    image_label1.configure(image=tk_image1)
    image_label2.configure(image=tk_image2)

    image_label1.image = tk_image1
    image_label2.image = tk_image2


def startPauseDiaporama():
    global diaporama_running
    if (diaporama_running):
        diaporama_running = False
    else:
        diaporama_running = True
        setTimerDisplayer()

def setTimerDisplayer():
    if (loaded_image1 != 0) and (loaded_image2 != 0):
        random_choice_1 = random.choice(image_files)
        random_choice_2 = random.choice(image_files)
        while(random_choice_1 == random_choice_2):
            random_choice_1 = random.choice(image_files)
            random_choice_2 = random.choice(image_files)

        load_images(random.choice(image_files), random.choice(image_files))
        display_images()

    if (diaporama_running):
        root.after(diaporama_interval, setTimerDisplayer)


def key_pressed_left(event):
    print("left")

def key_pressed_right(event):
    print("right")
    
def key_pressed(event):
    print("KeyPressed : ", event.char)
    if (event.char == 'd'):
        startPauseDiaporama()
    if (event.char == 'r'):
        load_images(random.choice(image_files), random.choice(image_files))
        display_images()


diaporama_interval = 10000
diaporama_running = False

# Création de la fenêtre principale
root = tk.Tk()
root.title("Sélection de fichier")

width= root.winfo_screenwidth()
height= root.winfo_screenheight()
#setting tkinter window size
root.geometry("%dx%d" % (width, height))

# Création du menu
menu_bar = tk.Menu(root)
root.config(menu=menu_bar)

# Création du menu "File"
file_menu = tk.Menu(menu_bar, tearoff=0)
menu_bar.add_cascade(label="File", menu=file_menu)

# Ajout des options dans le menu "File"
file_menu.add_command(label="Open Folder", command=open_folder)
file_menu.add_separator()
file_menu.add_command(label="Exit", command=root.quit)


image_label1 = tk.Label(root)
image_label2 = tk.Label(root)
image_label1.pack(side=tk.LEFT, expand=True, fill=tk.BOTH)
image_label2.pack(side=tk.RIGHT, expand=True, fill=tk.BOTH)

window_width, window_height = 0, 0

image_files = []
loaded_image1 = 0
loaded_image2 = 0

root.bind("<Configure>", resize)
root.bind("<Key>", key_pressed)
root.bind("<Left>", key_pressed_left)
root.bind("<Right>", key_pressed_right)

root.mainloop()